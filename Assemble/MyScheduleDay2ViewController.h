//
//  MyScheduleDayTableViewController.h
//  Group Schedule
//
//  Created by Lion User on 01/10/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyScheduleDayViewController : UIViewController
@property (strong, nonatomic) NSMutableArray *myScheduleArray;
@property (strong, nonatomic) NSArray *hourLabels;

@end
