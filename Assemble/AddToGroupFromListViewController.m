//
//  AddToGroupFromListViewController.m
//  Assemble
//
//  Created by Lion User on 15/10/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import "AddToGroupFromListViewController.h"

@interface AddToGroupFromListViewController ()

@end

@implementation AddToGroupFromListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
