//
//  MyScheduleDayTableViewController.m
//  Group Schedule
//
//  Created by Lion User on 01/10/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import "MyScheduleDayTableViewController.h"
#import "AppDelegate.h"

@interface MyScheduleDayTableViewController ()

@end

@implementation MyScheduleDayTableViewController

AppDelegate *aD;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}




- (void)viewWillAppear:(BOOL)animated
{
//    int myInt = 0;
//    int day = 06; // Temporary constant for Monday's array
//    
//    
//    
//    for (int hour = 0; hour < 24; hour++) {
//        
//        // NSLog(@"%i", [[[aD.myScheduleArray objectAtIndex:day] objectAtIndex:hour] intValue]);
//        
//        myInt = [[[aD.myScheduleArray objectAtIndex:day] objectAtIndex:hour] intValue];
//        
//        if (myInt == 1) {
//            
//        }
//        else {
//            
//        }
//    }

    
}





- (void)viewDidLoad
{
    [super viewDidLoad];
    
    aD = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.hourLabels = [[NSArray alloc] initWithObjects:@"1am", @"2am", @"3am", @"4am", @"5am", @"6am", @"7am", @"8am", @"9am", @"10am", @"11am", @"Midday", @"1pm", @"2pm", @"3pm", @"4pm", @"5pm", @"6pm", @"7pm", @"8pm", @"9pm", @"10pm", @"11pm", @"Midnight", nil];
    
    
    
}





- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}





- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}





#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}





- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    // NSLog(@"%i", [[aD.myScheduleArray objectAtIndex:0] count] + 1);
    return [[aD.myScheduleArray objectAtIndex:0] count];
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    
    int cellValue = [[[aD.myScheduleArray objectAtIndex:0] objectAtIndex:indexPath.row] intValue];
    // NSLog(@"%i", cellValue);
    
    
    
    cell.textLabel.text = [self.hourLabels objectAtIndex:indexPath.row];
    
    return cell;
}





- (UITableViewCell *)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    int cellValue = [[[aD.myScheduleArray objectAtIndex:0] objectAtIndex:indexPath.row] intValue];
    
    if (cellValue) cell.backgroundColor = [UIColor colorWithRed:0.2 green:0.3 blue:0.8 alpha:0.7];
    else cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@", [aD.myScheduleArray objectAtIndex:0]);
    
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    int cellValue = [[[aD.myScheduleArray objectAtIndex:0] objectAtIndex:indexPath.row] intValue];

    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    
if (cellValue ==  0) {
    [[aD.myScheduleArray objectAtIndex:0] replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:YES]];
    NSLog(@"%@", [aD.myScheduleArray objectAtIndex:0]);
    cell.backgroundColor = [UIColor colorWithRed:0.2 green:0.3 blue:0.8 alpha:0.7];    
}
if (cellValue ==  1) {
    [[aD.myScheduleArray objectAtIndex:0] replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:NO]];
    NSLog(@"%@", [aD.myScheduleArray objectAtIndex:0]);
    cell.backgroundColor = [UIColor whiteColor];
}
    
    [self.tableView reloadData];
    
}

@end
