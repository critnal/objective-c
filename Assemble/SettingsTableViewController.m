//
//  SettingsTableViewController.m
//  Assemble
//
//  Created by Lion User on 23/10/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "AppDelegate.h"

@interface SettingsTableViewController ()

@end

@implementation SettingsTableViewController

AppDelegate *appDelegate;






- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)receivedNotification:(NSNotification *)notification {
    [self.nameField resignFirstResponder];
    [self.userIDField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}





- (void)viewWillAppear:(BOOL)animated {
    [self nameField].text = [appDelegate.appUser valueForKey:@"name"];
    [self userIDField].text = [appDelegate.appUser valueForKey:@"id"];
}






- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 2;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 200, 300, 244)];
    tempView.backgroundColor = [UIColor clearColor];
    
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 300, 44)];
    tempLabel.backgroundColor = [UIColor clearColor];
    tempLabel.shadowColor = [UIColor blackColor];
    tempLabel.shadowOffset = CGSizeMake(0, 2);
    tempLabel.textColor = [UIColor whiteColor];
    tempLabel.font = [UIFont fontWithName:@"Helvetica" size:17];
    tempLabel.text = @"My Info";
    
    [tempView addSubview:tempLabel];
    
    return tempView;
}

//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//    
//    // Configure the cell...
//    
//    return cell;
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//}






- (IBAction)nameFieldBeganEditing:(id)sender {
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= 100;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}


- (IBAction)nameFieldChanged:(id)sender {
    [appDelegate.appUser setObject:[self nameField].text forKey:@"name"];
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += 100;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}



- (IBAction)userIDFieldBeganEditing:(id)sender {
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= 100;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}



- (IBAction)clipboardButtonPressed:(id)sender {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = [appDelegate.appUser valueForKey:@"id"];
    NSString *copyMessageText = [[NSString alloc] initWithFormat:@"UserID: %@", [appDelegate.appUser valueForKey:@"id"]];
    UIAlertView *copyMessage = [[UIAlertView alloc] initWithTitle:@"Copied to Clipboard" message:copyMessageText delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [copyMessage show];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


@end
