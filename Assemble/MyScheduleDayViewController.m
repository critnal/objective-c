//
//  MyScheduleDayViewController.m
//  Assemble!
//
//  Created by Alexander Crichton on 2/10/12.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import "MyScheduleDayViewController.h"
#import "AppDelegate.h"
#import <Parse/Parse.h>



@implementation MyScheduleDayViewController

@synthesize tableView = _tableView;
@synthesize dayIndex;
@synthesize selectedDayControl;

AppDelegate *appDelegate;
int dayIndex;
NSArray *indexPathArray;
NSMutableArray *myUnsavedSchedule;





- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}





- (void)viewWillAppear:(BOOL)animated
{
    selectedDayControl.selectedSegmentIndex = dayIndex;    
    myUnsavedSchedule = [[NSMutableArray alloc] initWithArray:[appDelegate deepCopySchedule:[appDelegate.appUser valueForKey:@"schedule"]]];
}





- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}





- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.hourLabels = [[NSArray alloc] initWithObjects:@"Midnight - 1am", @"1am - 2am", @"2am - 3am", @"3am - 4am", @"4am - 5am", @"5am - 6am", @"6am - 7am", @"7am - 8am", @"8am - 9am", @"9am - 10am", @"10am - 11am", @"11am - Midday", @"Midday - 1pm", @"1pm - 2pm", @"2pm - 3pm", @"3pm - 4pm", @"4pm - 5pm", @"5pm - 6pm", @"6pm - 7pm", @"7pm - 8pm", @"8pm - 9pm", @"9pm - 10pm", @"10pm - 11pm", @"11pm - Midnight", nil];    
}





- (void)viewDidUnload
{
    [self setTableView:nil];
    [self setSelectedDayControl:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}





- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}





#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 24;
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
        
    cell.textLabel.text = [self.hourLabels objectAtIndex:indexPath.row];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    return cell;
}





- (UITableViewCell *)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    int cellValue = [[[myUnsavedSchedule objectAtIndex:dayIndex] objectAtIndex:indexPath.row] intValue];
    
    if (cellValue) cell.backgroundColor = [UIColor colorWithRed:0.2 green:0.3 blue:0.8 alpha:0.7];
    else cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

#pragma mark - Table view delegate





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    int cellValue = [[[myUnsavedSchedule  objectAtIndex:dayIndex] objectAtIndex:indexPath.row] intValue];
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if (cellValue ==  0) {
        [[myUnsavedSchedule  objectAtIndex:dayIndex] replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInteger:1]];
        cell.backgroundColor = [UIColor colorWithRed:0.2 green:0.3 blue:0.8 alpha:0.7];
    }
    if (cellValue ==  1) {
        [[myUnsavedSchedule  objectAtIndex:dayIndex] replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInteger:0]];
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    indexPathArray = [NSArray arrayWithObject:indexPath];
    [tableView reloadRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationNone];
}





- (IBAction)selectedDayControlChanged:(id)sender {

    dayIndex = selectedDayControl.selectedSegmentIndex;
    [self.tableView reloadData];
}






- (IBAction)saveButtonPressed:(id)sender {
    [appDelegate.appUser setObject:myUnsavedSchedule forKey:@"schedule"];
    
    // Save the Schedule to Parse
    PFQuery *query = [PFQuery queryWithClassName:@"UserData"];
    [query getObjectInBackgroundWithId:[appDelegate.appUser objectForKey:@"id"] block:^(PFObject *object, NSError *error) {
        if (!error) {
            [object setObject:[appDelegate.appUser objectForKey:@"schedule"] forKey:@"schedule"];
            [object saveEventually];
        } else {
            NSLog(@"Error fetching object: %@", error);
        }
    }];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
    
     
    
    
}

@end

