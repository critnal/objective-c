//
//  EditGroupViewController.h
//  Assemble
//
//  Created by Lion User on 15/10/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditGroupViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableDictionary *selectedGroup;
@property (strong, nonatomic) NSIndexPath *selectedGroupIndexPath;
@property (weak, nonatomic) IBOutlet UITextField *groupNameField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;

@end
