//
//  MyScheduleDayViewController.h
//  Assemble!
//
//  Created by Alexander Crichton on 2/10/12.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyScheduleDayViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *friends;
@property (strong, nonatomic) NSArray *hourLabels;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (readwrite) int dayIndex;
@property (weak, nonatomic) IBOutlet UISegmentedControl *selectedDayControl;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;

@end
