//
//  EditGroupViewController.m
//  Assemble
//
//  Created by Lion User on 15/10/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import "EditGroupViewController.h"
#import "AppDelegate.h"

@implementation EditGroupViewController

@synthesize tableView = _tableView;
@synthesize selectedGroupIndexPath, groupNameField;

AppDelegate *appDelegate;
NSMutableDictionary *unsavedGroup;





- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}





- (void)viewDidLoad
{
    [super viewDidLoad];
	
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}




- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    unsavedGroup = [[NSMutableDictionary alloc] initWithDictionary:[appDelegate deepCopyGroup:[appDelegate.groups objectAtIndex:selectedGroupIndexPath.row]]];
    [groupNameField setText:[unsavedGroup valueForKey:@"name"]];
}





- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {        
        return [[unsavedGroup valueForKey:@"members"] count];
    }
    else
        return [appDelegate.friends count];
}





- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) 
        return @"Members";
    else
        return @"Friends";
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.section == 0) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.textLabel.text = [[[unsavedGroup valueForKey:@"members"] objectAtIndex:indexPath.row] valueForKey:@"name"];
        
    }
    else {
        cell.textLabel.text = [[appDelegate.friends objectAtIndex:indexPath.row] valueForKey:@"name"];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        for (NSMutableDictionary *dictionary in [unsavedGroup valueForKey:@"members"]) {
            if ([[dictionary valueForKey:@"name"] isEqual:[[appDelegate.friends objectAtIndex:indexPath.row] valueForKey:@"name"]]) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
        }
        
    }
        
    return cell;
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {        
        [[unsavedGroup valueForKey:@"members"] removeObjectAtIndex:indexPath.row];
    }
    
    else if ([[unsavedGroup valueForKey:@"members"] count] == 0) {        
        [[unsavedGroup valueForKey:@"members"] addObject:[appDelegate.friends objectAtIndex:indexPath.row]];        
        [[unsavedGroup valueForKey:@"members"] setArray:[appDelegate sortContents:[unsavedGroup valueForKey:@"members"]]];
    }
    
    
    else {        
        BOOL hasSameName = NO;
        NSIndexPath *indexToRemove;
        
        
        for (NSMutableDictionary *dictionary in [unsavedGroup valueForKey:@"members"]) {            
            if ([[dictionary valueForKey:@"name"] isEqual:[[appDelegate.friends objectAtIndex:indexPath.row] valueForKey:@"name"]]) {
                hasSameName = YES;
                indexToRemove = [NSIndexPath  indexPathForRow:[[unsavedGroup valueForKey:@"members"] indexOfObject:dictionary] inSection:1];
                
                
            }
        }
            if (hasSameName == NO) {            
                [[unsavedGroup valueForKey:@"members"] addObject:[appDelegate.friends objectAtIndex:indexPath.row]];                
                [[unsavedGroup valueForKey:@"members"] setArray:[appDelegate sortContents:[unsavedGroup valueForKey:@"members"]]];
            }
            else
                [[unsavedGroup valueForKey:@"members"] removeObjectAtIndex:indexToRemove.row];        
    }        
    [tableView reloadData];
}





- (IBAction)groupNameFieldChanged:(id)sender {
    [unsavedGroup setObject:groupNameField.text forKey:@"name"];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}





- (IBAction)saveButtonPressed:(id)sender {
    [appDelegate.groups replaceObjectAtIndex:selectedGroupIndexPath.row withObject:unsavedGroup];
    [self.navigationController popViewControllerAnimated:YES];
}


@end
