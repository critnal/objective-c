//
//  GroupsViewController.h
//  Assemble
//
//  Created by Lion User on 14/10/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILongPressGestureRecognizer *longPress;

@end
