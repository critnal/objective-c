//
//  GroupsScheduleViewController.m
//  Assemble
//
//  Created by Lion User on 14/10/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import "GroupsScheduleViewController.h"
#import "AppDelegate.h"

@implementation GroupsScheduleViewController

@synthesize selectedGroupIndexPath;

UILabel *labelsArray[7][24];
int scheduleCount[7][24];
AppDelegate *appDelegate;
UIImageView *overlayImageview;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}






- (void)viewDidUnload {
    [super viewDidUnload];
}






- (void)viewWillAppear:(BOOL)animated {
    
    [self.navigationBar setTitle:[[appDelegate.groups objectAtIndex:selectedGroupIndexPath.row] valueForKey:@"name"]];
    
    // Reset the array that tallys user schedule hours
    for (int i = 0; i < 7; i++) {
        for (int j = 0; j < 24; j++) {
            scheduleCount[i][j] = 0;
        }
    }
    
    
    
    // Tally the app user's schedule
    for (int day = 0; day < 7; day++) {
        for (int hour = 0; hour < 24; hour++) {
            
            if (([[[[appDelegate.appUser valueForKey:@"schedule"] objectAtIndex:day] objectAtIndex:hour] intValue] == 1)
                &&
                (scheduleCount[day][hour] <= 5))
            {
                scheduleCount[day][hour]++;
            }
        }
    }
    
    
    
    
    
    // Tally all the schedules of the selected users
    for (NSDictionary *dictionary in [[appDelegate.groups objectAtIndex:selectedGroupIndexPath.row] valueForKey:@"members"]) {
        
        for (int day = 0; day < 7; day++) {
            for (int hour = 0; hour < 24; hour++) {
                
                if (([[[[dictionary valueForKey:@"schedule"] objectAtIndex:day] objectAtIndex:hour] intValue] == 1)
                    &&
                    (scheduleCount[day][hour] <= 5))
                {
                    scheduleCount[day][hour]++;
                }
            }
        }
    }
    
    
    // Colour all the labels according to their tally
    int temp;
    for (int day = 0; day < 7; day++) {
        for (int hour = 0; hour < 24; hour++) {
            
            temp = scheduleCount[day][hour];
            NSLog(@"%i", temp);
//             Orange Colours
//            if (temp == 1) {
//                labelsArray[day][hour].backgroundColor = [UIColor colorWithRed:1 green:0.486 blue:0.149 alpha:1];
//            }
//            else if (temp == 2) {
//                labelsArray[day][hour].backgroundColor = [UIColor colorWithRed:0.847 green:0.352 blue:0.055 alpha:1];
//            }
//            else if (temp >= 3) {
//                labelsArray[day][hour].backgroundColor = [UIColor colorWithRed:0.725 green:0.184 blue:0.015 alpha:1];
//            }
            
            if (temp == 1) {
                labelsArray[day][hour].backgroundColor = [UIColor colorWithRed:0.176 green:0.635 blue:0.905 alpha:1];
            }
            else if (temp == 2) {
                labelsArray[day][hour].backgroundColor = [UIColor colorWithRed:0.078 green:0.505 blue:0.761 alpha:1];
            }
            else if (temp >= 3) {
                labelsArray[day][hour].backgroundColor = [UIColor colorWithRed:0 green:0.357 blue:0.568 alpha:1];
            }

        }
    }
    
    [self.overlayView addSubview:overlayImageview];
    [super viewWillAppear:animated];
}





- (void)viewDidLoad {
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    overlayImageview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"calendar_view_new_2.png"]];
    overlayImageview.frame = CGRectMake(0, 0, 320, 367);
    
    
    for (int day = 0; day < 7; day++) {
        int hourPixel = 0;
        
        for (int hour = 0; hour < 24; hour++) {
            
            if ((hour%3 == 0) && (hour > 1)) hourPixel++;
            if ((hour%6 == 0) && (hour > 1)) hourPixel++;
            
            labelsArray[day][hour] = [[UILabel alloc] initWithFrame:CGRectMake(11 + 43 * day, 34 + 13 * hour + hourPixel, 41, 13)];
            labelsArray[day][hour].backgroundColor = [UIColor whiteColor];
            
            labelsArray[day][hour].alpha = 1;
            
            [self.view addSubview:labelsArray[day][hour]];
        }
    }
    
    
}

@end