//
//  FriendsListTableViewController.m
//  Assemble
//
//  Created by Lion User on 10/10/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import "FriendsListTableViewController.h"
#import "AppDelegate.h"

@implementation FriendsListTableViewController





AppDelegate *appDelegate;
NSArray *sortedFriendsArray, *groupsArray;





- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}





- (void)viewDidLoad
{
    [super viewDidLoad];

    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sortedFriendsArray = [[NSMutableArray alloc] init];
    groupsArray = [[NSMutableArray alloc] init];
}





- (void)viewDidUnload
{
    [super viewDidUnload];
}





- (void)viewWillAppear:(BOOL)animated
{
    sortedFriendsArray = [appDelegate.friends sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first  = [(NSDictionary *)a valueForKey:@"name"];
        NSString *second = [(NSDictionary *)b valueForKey:@"name"];
        return [first compare:second];
    }];
    
}





- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    
    return cell;
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
