//
//  AppDelegate.h
//  Assemble!
//
//  Created by Lion User on 02/09/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (retain, nonatomic) NSMutableArray *friends, *groups;
@property (retain, nonatomic) NSMutableDictionary *appUser;
@property (readwrite) NSNumber *appUserID, *userIDCount;

- (NSMutableArray *)sortContents:(NSMutableArray *)arrayToSort;
- (NSMutableArray *)deepCopySchedule:(NSMutableArray *)scheduleToCopy;
- (NSMutableDictionary *)deepCopyGroup:(NSMutableDictionary *)groupToCopy;
- (NSString *)getNewUserID;

@end
