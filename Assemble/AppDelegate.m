//
//  AppDelegate.m
//  Assemble!
//
//  Created by Lion User on 02/09/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>

@implementation AppDelegate

@synthesize appUser, friends, groups, userIDCount;

NSString *destinationPath, *documentDirectoryPath, *sourcePath, *savePath;
NSArray *pathsArray;

NSFileManager *fileManager;





- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    fileManager = [NSFileManager defaultManager];
    pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    
    documentDirectoryPath = [pathsArray objectAtIndex:0];
    NSError *error;
    
    
    // Get the appUser plist
    destinationPath = [documentDirectoryPath stringByAppendingPathComponent:@"appUser.plist"];
    appUser = [[NSMutableDictionary alloc] initWithContentsOfFile:destinationPath];
    
    if (appUser == nil) {
        sourcePath =[[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:@"appUser.plist"];
        [fileManager copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        appUser = [[NSMutableDictionary alloc] initWithContentsOfFile:destinationPath];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter your name" message:@"  " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        [alert show];
    }
    
    
    // Get the friends plist
    destinationPath = [documentDirectoryPath stringByAppendingPathComponent:@"friends.plist"];
    friends = [[NSMutableArray alloc] initWithContentsOfFile:destinationPath];
    
    if (friends == nil) {  
        sourcePath =[[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:@"friends.plist"];
        [fileManager copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        friends = [[NSMutableArray alloc] initWithContentsOfFile:destinationPath];
    }    
    
    
        
    // Get the groups plist
    destinationPath = [documentDirectoryPath stringByAppendingPathComponent:@"groups.plist"];
    groups = [[NSMutableArray alloc] initWithContentsOfFile:destinationPath];
    
    if (groups == nil) {
        sourcePath =[[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:@"groups.plist"];
        [fileManager copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        groups = [[NSMutableArray alloc] initWithContentsOfFile:destinationPath];
    }
    
    userIDCount = [NSNumber numberWithInt:[friends count]];
    
    
    
    [Parse setApplicationId:@"hi1JiiqG3eohbbktyoJSUprS2gJUHO0pL1g1Jp3S"
                  clientKey:@"Yp5qnl9JW7F1CunxKQBs4YrdNPyPErRzj8PNwWy2"];
    
    // If no user ID then create a new parse object and save to parse
    if ([[appUser valueForKey:@"id"] length] == 0) {
        PFObject *userData = [PFObject objectWithClassName:@"UserData"];
        [userData setObject:[appUser objectForKey:@"schedule"] forKey:@"schedule"];
        [userData saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                [userData refresh];
                [appUser setObject:userData.objectId forKey:@"id"];
            } else {
                NSLog(@"Error saving object: %@", error);
            }
        }];
    }
    
    // Update all friends data from Parse Cloud
    for (NSMutableDictionary *friend in friends) {
        PFQuery *query = [PFQuery queryWithClassName:@"UserData"];
        [query getObjectInBackgroundWithId:[friend objectForKey:@"id"]
                                     block:^(PFObject *friendData, NSError *error) {
                                         if (!error) {
                                             NSLog(@"Fetching data for: %@", [friend objectForKey:@"id"]);
                                             [friend setObject: [friendData objectForKey:@"schedule"] forKey:@"schedule"];
                                         } else {
                                             NSLog(@"Error fetching object: %@", error);
                                         }
                                     }];
        
    }
    
    return YES;
}





- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [appUser setObject:[alertView textFieldAtIndex:0].text forKey:@"name"];
    }
}










- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    documentDirectoryPath = [pathsArray objectAtIndex:0];
    
    savePath = [documentDirectoryPath stringByAppendingPathComponent:@"appUser.plist"];
    [appUser writeToFile:savePath atomically:YES];
    
    savePath = [documentDirectoryPath stringByAppendingPathComponent:@"friends.plist"];
    [friends writeToFile:savePath atomically:YES];
    
    savePath = [documentDirectoryPath stringByAppendingPathComponent:@"groups.plist"];
    [groups writeToFile:savePath atomically:YES];
}





- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    
    
    pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    documentDirectoryPath = [pathsArray objectAtIndex:0];
    
    savePath = [documentDirectoryPath stringByAppendingPathComponent:@"appUser.plist"];
    [appUser writeToFile:savePath atomically:YES];
    
    savePath = [documentDirectoryPath stringByAppendingPathComponent:@"friends.plist"];
    [friends writeToFile:savePath atomically:YES];
    
    savePath = [documentDirectoryPath stringByAppendingPathComponent:@"groups.plist"];
    [groups writeToFile:savePath atomically:YES];
}





- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}





- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    documentDirectoryPath = [pathsArray objectAtIndex:0];
    
    savePath = [documentDirectoryPath stringByAppendingPathComponent:@"appUser.plist"];
    [appUser writeToFile:savePath atomically:YES];
    
    savePath = [documentDirectoryPath stringByAppendingPathComponent:@"friends.plist"];
    [friends writeToFile:savePath atomically:YES];
    
    savePath = [documentDirectoryPath stringByAppendingPathComponent:@"groups.plist"];
    [groups writeToFile:savePath atomically:YES];
}





- (void)initialAlert {
}





- (NSMutableArray *)sortContents:(NSMutableArray *)arrayToSort
{
    arrayToSort = [[arrayToSort sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first  = [(NSDictionary *)a valueForKey:@"name"];
        NSString *second = [(NSDictionary *)b valueForKey:@"name"];
        return [first caseInsensitiveCompare:second];
    }] mutableCopy];
    
    return arrayToSort;
}





- (NSMutableArray *)deepCopySchedule:(NSMutableArray *)scheduleToCopy {
    
    NSMutableArray *scheduleToReturn = [[NSMutableArray alloc] init];
    for (int day = 0; day < 7; day++) {
        NSMutableArray *tempDayArray = [[NSMutableArray alloc] init];
        
        for (int hour = 0; hour < 24; hour++) {
            NSNumber *tempNumber = [[NSNumber alloc] initWithInteger:[[[scheduleToCopy objectAtIndex:day] objectAtIndex:hour] integerValue]];
            [tempDayArray addObject:tempNumber];
        }
        
        [scheduleToReturn addObject:tempDayArray];
    }
    return scheduleToReturn;
}





- (NSMutableDictionary *)deepCopyGroup:(NSMutableDictionary *)groupToCopy {
            
    NSMutableArray *groupToReturnObjects = [[NSMutableArray alloc] init];
    NSMutableArray *groupToReturnKeys = [[NSMutableArray alloc] initWithObjects:@"name", @"members", nil ];
    
    
    // Copy the group's name
    NSString *groupNameToReturn = [NSString stringWithString:[groupToCopy valueForKey:@"name"]];
    [groupToReturnObjects addObject:groupNameToReturn];
    
    
    // Copy each friend in the group to groupMembersToReturn
    NSMutableArray *groupMembersToReturn = [[NSMutableArray alloc] init];
    
    for (NSMutableDictionary *friendDictToCopy in [groupToCopy valueForKey:@"members"]) {
        
        NSString *friendIDToReturn = [NSString stringWithString:[friendDictToCopy valueForKey:@"id"]];
        
        NSString *friendNameToReturn = [NSString stringWithString:[friendDictToCopy valueForKey:@"name"]];        
              
        NSMutableArray *friendScheduleToReturn = [[NSMutableArray alloc] init];
        friendScheduleToReturn = [self deepCopySchedule:[friendDictToCopy valueForKey:@"schedule"]];
        
        NSMutableArray *friendDictToReturnObjects = [[NSMutableArray alloc] initWithObjects:friendIDToReturn, friendNameToReturn, friendScheduleToReturn, nil];
        NSMutableArray *friendDictToReturnKeys = [[NSMutableArray alloc] initWithObjects:@"id", @"name", @"schedule", nil];
        
        NSMutableDictionary *friendToReturn = [[NSMutableDictionary alloc] initWithObjects:friendDictToReturnObjects forKeys:friendDictToReturnKeys];
        
        [groupMembersToReturn addObject:friendToReturn];
    }
    [groupToReturnObjects addObject:groupMembersToReturn];
    
    
    // Insert group objects at keys
    NSMutableDictionary *groupToReturn = [[NSMutableDictionary alloc] initWithObjects:groupToReturnObjects forKeys:groupToReturnKeys];
    
    return groupToReturn;
}





- (NSString *)getNewUserID
{
    userIDCount = [NSNumber numberWithInt:[userIDCount integerValue] + 1];
    return [NSString stringWithFormat:@"%@", userIDCount];
    
}

@end
