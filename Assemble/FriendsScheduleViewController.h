//
//  FriendsScheduleViewController.h
//  Assemble
//
//  Created by Lion User on 12/10/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsScheduleViewController : UIViewController

@property (strong, nonatomic) NSMutableDictionary *selectedFriend;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBar;

@property (strong, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIView *labelsView;

@end
