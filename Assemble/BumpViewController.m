//
//  BumpViewController.m
//  Assemble
//
//  Created by Temmink on 23/10/12.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import "BumpViewController.h"
#import "BumpClient.h"
#import "AppDelegate.h"
#import <Parse/Parse.h>

@implementation BumpViewController



AppDelegate *appDelegate;
NSArray *tempDictObjects, *tempDictKeys;
NSMutableDictionary *selectedFriend, *tempDictionary;
NSString *friendName, *friendId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    [self configureBump];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)BumpButtonPressed:(id)sender {
    [[BumpClient sharedClient] simulateBump];
}

- (void) configureBump {
    
    [BumpClient configureWithAPIKey:@"7c5ceff27fb74ff8b259fd531b068075" andUserID:[[UIDevice currentDevice] name]];
    
    [[BumpClient sharedClient] setMatchBlock:^(BumpChannelID channelID) {
        [self statusLabel].text = [NSString stringWithFormat:@"Bump with %@!", [[BumpClient sharedClient] userIDForChannel:channelID]];
        NSLog(@"Matched with user: %@", [[BumpClient sharedClient] userIDForChannel:channelID]);
        [[BumpClient sharedClient] confirmMatch:YES onChannel:channelID];
        
    }];
    
    [[BumpClient sharedClient] setChannelConfirmedBlock:^(BumpChannelID channelID) {
        NSLog(@"Channel with %@ confirmed.", [[BumpClient sharedClient] userIDForChannel:channelID]);
        
        NSString *tempData = [[NSString alloc] initWithFormat: @"%@,%@",[appDelegate.appUser objectForKey:@"id" ],[appDelegate.appUser objectForKey:@"name"]];
        
        [[BumpClient sharedClient] sendData:[tempData dataUsingEncoding:NSUTF8StringEncoding] toChannel:channelID];
        
    }];
    
    [[BumpClient sharedClient] setDataReceivedBlock:^(BumpChannelID channel, NSData *data) {
        //NSLog(@"Data receieved from %@: %@", [[BumpClient sharedClient] userIDForChannel:channel],[NSString stringWithCString:[data bytes] encoding:NSUTF8StringEncoding]);
              
        NSArray *tempArray = [[NSString stringWithCString:[data bytes] encoding:NSUTF8StringEncoding] componentsSeparatedByCharactersInSet:
                               [NSCharacterSet characterSetWithCharactersInString:@","]
                               ];
        
        friendId = tempArray[0];
        friendName = tempArray[1];
        
        NSLog(@"Data received: %@ / %@", friendId, friendName);
        
        [self addFriendWithBump];
        
    }];
    
    [[BumpClient sharedClient] setConnectionStateChangedBlock:^(BOOL connectedToBumpServer) {
        if (connectedToBumpServer) {
            NSLog(@"Bump connected...");
            [self statusLabel].text = @"Ready to Bump!";
        } else {
            NSLog(@"Bump disconnected...");
        }
    }];
    
    [[BumpClient sharedClient] setBumpEventBlock:^(bump_event event) {
        switch(event) {
            case BUMP_EVENT_BUMP:
                NSLog(@"Bump detected");
                break;
            case BUMP_EVENT_NO_MATCH:
                NSLog(@"No match.");
                [self statusLabel].text = @"No match found, try again.";
                break;
        }
    }];
    
    [[BumpClient sharedClient] setBumpable:NO];
    
}

- (void) addFriendWithBump {
    
    PFQuery *query = [PFQuery queryWithClassName:@"UserData"];
    [query getObjectInBackgroundWithId:friendId
                                 block:^(PFObject *friendData, NSError *error) {
                                     if (!error) {
                                         NSLog(@"Fetching data for: %@", friendId);
                                         
                                         // Prepare generated friend's dictionary
                                         tempDictObjects = [[NSArray alloc] initWithObjects: friendId, friendName, [friendData objectForKey:@"schedule"], nil];
                                         tempDictKeys = [[NSArray alloc] initWithObjects:@"id", @"name", @"schedule", nil];
                                         tempDictionary = [[NSMutableDictionary alloc] initWithObjects:tempDictObjects forKeys:tempDictKeys];
                                         
                                         // Save
                                         [appDelegate.friends addObject:tempDictionary];
                                         
                                         // Sort
                                         appDelegate.friends = [appDelegate sortContents:appDelegate.friends];
                                         
                                         // Segue back to friends view
                                         [self.navigationController popViewControllerAnimated:YES];
                                         
                                     } else {
                                         NSLog(@"Error fetching object: %@", error);
                                     }
                                 }];
    
}

@end
