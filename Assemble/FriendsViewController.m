//
//  FriendsViewController.m
//  Assemble
//
//  Created by Lion User on 10/10/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import "FriendsViewController.h"
#import "FriendsScheduleViewController.h"
#import <Parse/Parse.h>
#import "AppDelegate.h"

@implementation FriendsViewController

@synthesize tableView = _tableView;
@synthesize addFriendButton = _addFriendButton;
@synthesize bumpFriendButton = _bumpFriendButton;

AppDelegate *appDelegate;
NSArray *tempDictObjects, *tempDictKeys;
NSMutableDictionary *selectedFriend, *tempDictionary;
NSString *alphabet;





- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}





- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    selectedFriend = [[NSMutableDictionary alloc] init];
    
    alphabet = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";
}





- (void)viewDidUnload
{
    [self setAddFriendButton:nil];
    [self setBumpFriendButton:nil];
    [super viewDidUnload];
}





- (void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
    [super viewWillAppear:animated];
}





- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [appDelegate.friends count];
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    
        cell.textLabel.text = [[appDelegate.friends objectAtIndex:indexPath.row] valueForKey:@"name"];
    
        return cell;
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedFriend = [appDelegate.friends objectAtIndex:indexPath.row];
        
    [self performSegueWithIdentifier:@"segueToFriendsSchedule" sender:nil];
}





- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedFriend = [appDelegate.friends objectAtIndex:indexPath.row];
    [appDelegate.friends removeObjectAtIndex:indexPath.row];
    
    for (NSMutableDictionary *group in appDelegate.groups) {
        [[group objectForKey:@"members"] removeObject:selectedFriend];
    }
    
}





- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}





- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView reloadData];
}





- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"segueToFriendsSchedule"]) {
        // Tell the combined schedule what friend it needs
        FriendsScheduleViewController *destinationController = segue.destinationViewController;
        destinationController.selectedFriend = selectedFriend;
    }
}


@end
