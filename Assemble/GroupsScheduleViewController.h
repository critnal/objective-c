//
//  GroupsScheduleViewController.h
//  Assemble
//
//  Created by Lion User on 14/10/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupsScheduleViewController : UIViewController

@property (strong, nonatomic) NSIndexPath *selectedGroupIndexPath;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBar;

@property (strong, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIView *labelsView;

@end
