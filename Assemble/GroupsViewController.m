//
//  GroupsViewController.m
//  Assemble
//
//  Created by Lion User on 14/10/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import "GroupsViewController.h"
#import "GroupsScheduleViewController.h"
#import "EditGroupViewController.h"
#import "AppDelegate.h"

@implementation GroupsViewController
@synthesize tableView = _tableView;
@synthesize longPress;

AppDelegate *appDelegate;
NSIndexPath *selectedGroupIndexPath;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}





- (void)viewDidLoad
{
    [super viewDidLoad];
	
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
}





- (void)viewDidUnload
{
    [self setTableView:nil];
    [self setLongPress:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}





- (void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
    [super viewWillAppear:animated];
}





- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [appDelegate.groups count];
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"GroupCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.textLabel.text = [[appDelegate.groups objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%i",[[[appDelegate.groups objectAtIndex:indexPath.row] valueForKey:@"members"] count]];
    
    return cell;
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // IMPORTANT: this is a shallow copy. The objects in unsavedGroup ARE the objects
    // in appDelegate.groups. Performing [unsavedGroup removeAllObjects] will remove them from both arrays.
    selectedGroupIndexPath = indexPath;
    
    [self performSegueWithIdentifier:@"segueToGroupsSchedule" sender:nil];
}





- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [appDelegate.groups removeObjectAtIndex:indexPath.row];
}





- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}





- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView reloadData];
}






- (IBAction)longPressTableView:(id)sender {
    
    
    if (longPress.state == UIGestureRecognizerStateEnded) {
        
        CGPoint touchPoint = [sender locationInView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
        
        selectedGroupIndexPath = indexPath;
        [self performSegueWithIdentifier:@"segueToEditGroup" sender:nil];
    }
}







- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Change the back button to be more clear about its function
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:nil action:nil];
    [[self navigationItem] setBackBarButtonItem:backButton];
    
    
    if ([segue.identifier isEqualToString:@"segueToGroupsSchedule"]) {
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Groups" style:UIBarButtonItemStyleBordered target:nil action:nil];
        [[self navigationItem] setBackBarButtonItem:backButton];
        
        // Tell the combined schedule what friends it needs
        GroupsScheduleViewController *destinationController = segue.destinationViewController;
        destinationController.selectedGroupIndexPath = selectedGroupIndexPath;
    }
    
    else if ([segue.identifier isEqualToString:@"segueToEditGroup"]) {        // Tell the group edit view what group to edit
        EditGroupViewController *destinationController = segue.destinationViewController;
        destinationController.selectedGroupIndexPath = selectedGroupIndexPath;
    }
}

@end
