//
//  FriendsScheduleViewController.m
//  Assemble
//
//  Created by Lion User on 12/10/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import "FriendsScheduleViewController.h"
#import "AppDelegate.h"

@implementation FriendsScheduleViewController

@synthesize selectedFriend;

UILabel *labelsArray[7][24];
int scheduleCount[7][24];
AppDelegate *appDelegate;
NSMutableArray *appUserSchedule;
UIImage *overlayImage;
UIImageView *overlayImageView;





- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}






- (void)viewDidUnload {
    [super viewDidUnload];
}






- (void)viewWillAppear:(BOOL)animated {
    
    [self.navigationBar setTitle:[selectedFriend valueForKey:@"name"]];
    
    // Reset the array that tallys user schedule hours
    for (int i = 0; i < 7; i++) {
        for (int j = 0; j < 24; j++) {
            scheduleCount[i][j] = 0;
        }
    }
    
    
    
    
    // Tally the app user's schedule
    for (int day = 0; day < 7; day++) {
        for (int hour = 0; hour < 24; hour++) {
            
            if ([[[[appDelegate.appUser valueForKey:@"schedule"] objectAtIndex:day] objectAtIndex:hour] intValue] == 1)
            {
                scheduleCount[day][hour]++;
            }
        }
    }

    
    
    
    
    // Tally the schedule of the selected friend
    for (int day = 0; day < 7; day++) {
            for (int hour = 0; hour < 24; hour++) {
                
                if ([[[[selectedFriend valueForKey:@"schedule"] objectAtIndex:day] objectAtIndex:hour] intValue] == 1)
                {
                    scheduleCount[day][hour] = scheduleCount[day][hour] + 1;
                }
            }
        }
     
    
    // Colour all the labels according to their tally
    int temp;
    for (int day = 0; day < 7; day++) {        
        for (int hour = 0; hour < 24; hour++) {
            
            temp = scheduleCount[day][hour];
            
            if (temp == 1) labelsArray[day][hour].alpha = 0.4;
            else if (temp == 2) labelsArray[day][hour].alpha = 0.5;
        }
    }
    
    
    [super viewWillAppear:animated];
}





- (void)viewDidLoad {
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    overlayImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"calendar_view_new_2.png"]];
    overlayImageView.frame = CGRectMake(0, 0, 320, 367);
    
    for (int day = 0; day < 7; day++) {
        int hourPixel = 0;
        
        for (int hour = 0; hour < 24; hour++) {
            
            if ((hour%3 == 0) && (hour > 1)) hourPixel++;
            if ((hour%6 == 0) && (hour > 1)) hourPixel++;
            
            labelsArray[day][hour] = [[UILabel alloc] initWithFrame:CGRectMake(11 + 43 * day, 34 + 13 * hour + hourPixel, 41, 13)];
            labelsArray[day][hour].backgroundColor = [UIColor blueColor];
            
            labelsArray[day][hour].alpha = 0.0;
            
            [self.labelsView addSubview:labelsArray[day][hour]];
        }
    }
    
    [self.overlayView addSubview:overlayImageView];
}

@end
