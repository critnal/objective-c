//
//  BumpViewController.h
//  Assemble
//
//  Created by Temmink on 23/10/12.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BumpViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *BumpButton;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@end
