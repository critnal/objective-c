//
//  GroupEditorViewController.m
//  Assemble
//
//  Created by Lion User on 12/10/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import "NewGroupViewController.h"
#import "AppDelegate.h"

@implementation NewGroupViewController

@synthesize tableView = _tableView;
@synthesize createdGroup, groupNameField;

AppDelegate *appDelegate;





- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}





- (void)viewDidLoad
{
    [super viewDidLoad];
	
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}





- (void)viewWillAppear:(BOOL)animated
{    
    [self.tableView reloadData];
    [super viewWillAppear:animated];
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    NSString *tempString = @"New Group";
    
    NSMutableArray *tempDictObjects = [[NSMutableArray alloc] initWithObjects:tempString, tempArray, nil];
    
    NSMutableArray *tempDictKeys = [[NSMutableArray alloc] initWithObjects:@"name", @"members", nil];
    
    createdGroup = [[NSMutableDictionary alloc] initWithObjects:tempDictObjects forKeys:tempDictKeys];
}





- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [[createdGroup valueForKey:@"members"] count];
    }
    else
        return [appDelegate.friends count];
}






- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return @"Members";
    else
        return @"Friends";
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.section == 0) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.textLabel.text = [[[createdGroup valueForKey:@"members"] objectAtIndex:indexPath.row] valueForKey:@"name"];
        
    }
    else {
        cell.textLabel.text = [[appDelegate.friends objectAtIndex:indexPath.row] valueForKey:@"name"];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        for (NSMutableDictionary *dictionary in [createdGroup valueForKey:@"members"]) {
            if ([[dictionary valueForKey:@"name"] isEqual:[[appDelegate.friends objectAtIndex:indexPath.row] valueForKey:@"name"]]) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
        }
        
    }
    
    return cell;
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        [[createdGroup valueForKey:@"members"] removeObjectAtIndex:indexPath.row];
    }
    
    else if ([[createdGroup valueForKey:@"members"] count] == 0) {
        [[createdGroup valueForKey:@"members"] addObject:[appDelegate.friends objectAtIndex:indexPath.row]];
        [[createdGroup valueForKey:@"members"] setArray:[appDelegate sortContents:[createdGroup valueForKey:@"members"]]];
    }
    
    
    else {
        BOOL hasSameName = NO;
        NSIndexPath *indexToRemove;
        
        
        for (NSMutableDictionary *dictionary in [createdGroup valueForKey:@"members"]) {
            if ([[dictionary valueForKey:@"name"] isEqual:[[appDelegate.friends objectAtIndex:indexPath.row] valueForKey:@"name"]]) {
                hasSameName = YES;
                indexToRemove = [NSIndexPath  indexPathForRow:[[createdGroup valueForKey:@"members"] indexOfObject:dictionary] inSection:1];
                
                
            }
        }
        if (hasSameName == NO) {
            [[createdGroup valueForKey:@"members"] addObject:[appDelegate.friends objectAtIndex:indexPath.row]];
            [[createdGroup valueForKey:@"members"] setArray:[appDelegate sortContents:[createdGroup valueForKey:@"members"]]];
        }
        else
            [[createdGroup valueForKey:@"members"] removeObjectAtIndex:indexToRemove.row];
    }
    [tableView reloadData];
}






- (IBAction)groupNameFieldChanged:(id)sender {
    [createdGroup setObject:groupNameField.text forKey:@"name"];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}






- (IBAction)saveButtonPressed:(id)sender {
    [appDelegate.groups addObject:createdGroup];
    appDelegate.groups = [appDelegate sortContents:appDelegate.groups];
    [self.navigationController popViewControllerAnimated:YES];
}


@end
